package com.example.mycalculator;

import android.util.Log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class RuleAndResult {
    public static Stack Postfix(StringBuilder s)
    {   ArrayList<String> arr = new ArrayList<String>();
        Log.d("表达式",s.toString());
        Stack<String> numstack=new Stack<String>();   //存放操作数
        Stack<String> operstack=new Stack<String>();   //存放操作符
        //需要创建一个字符串存放数，不然2位数及以上会被拆开，不方便后续处理
        String temp="";
        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        hashMap.put("(", 0);// 设定优先级 +-优先级相同 */优先级相同
        hashMap.put("+", 1);
        hashMap.put("-", 1);
        hashMap.put("*", 2);
        hashMap.put("/", 2);

        for(int i=0;i<s.length();i++){
            char c=s.charAt(i);  //获取第i个位置的字符
            String m=c+"";
            //c为数字
            if(Character.isDigit(c)||c=='.'){
                //是否为最后一个字符
                if(i==s.length()-1){
                    temp+=m;
                    numstack.push(temp);
                    arr.add(temp);
                } else
                    //不是第一个位置，要看前一个是不是数字或小数点
                    temp+=m;

            }
                //如果为操作符则直接将temp数字放进num栈中
            else{
                if(temp!="") {
                    numstack.push(temp);
                    arr.add(temp);
                    temp="";
                }  //这个判断是为了不让括号后紧接着数字的情况出错
                if(c=='('){
                    operstack.push(m);
                } else if(c==')') {   //遇到右括号，输出运算符堆栈中的运算符到操作数堆栈，直到遇到左括号为止
                    while(!operstack.isEmpty()&&!operstack.peek().equals("(")){
                        String r = operstack.pop();
                        numstack.push(r);
                        arr.add(r);
                    }
                    if(operstack.peek().equals("("))operstack.pop();
                }
                //加减乘除的情况
                else {
                    //首先是加减，他们的优先级低于乘除，只有优先级大于等于栈顶才弹出
                    switch(c){
                        case '+':
                        case '-':
                            //若是不为空且操作栈顶元素的优先值大于一
                            if((!operstack.isEmpty())&&hashMap.get(operstack.peek())>1){
                                String t=operstack.pop();
                                numstack.push(t);
                                arr.add(t);
                                operstack.push(m);
                            }else{
                                operstack.push(m);
                            }
                            break;
                        case '*':
                        case '/':
                            operstack.push(m);
                            break;
                    }
                }
            }
        }
        while(!operstack.isEmpty()){
            String q=operstack.pop();
            numstack.push(q);
            arr.add(q);
        }
        Log.d("后缀表达式",arr.toString());
        return numstack;
    }

    public static String calcPostfix (Stack<String> numstack) throws Exception//计算逆波兰表达式
    {
        ArrayList<String> arr = new ArrayList<String>();
        while(!numstack.isEmpty()){
            String t=numstack.pop();
            arr.add(t);
        }
        ArrayList<String> arr1 = new ArrayList<String>();
        for(int i=arr.size()-1;i>=0;i--){
            int j=arr1.size();
            switch(arr.get(i)){
                case "":
                    break;
                case "+":
                    BigDecimal a=new BigDecimal(arr1.remove(j-2)).add(new BigDecimal(arr1.remove(j-2)));
                    arr1.add(String.valueOf(a));
                    break;
                case "-":
                    BigDecimal b=new BigDecimal(arr1.remove(j-2)).subtract(new BigDecimal(arr1.remove(j-2)));
                    arr1.add(String.valueOf(b));
                    break;
                case "*":
                    BigDecimal c=new BigDecimal(arr1.remove(j-2)).multiply(new BigDecimal(arr1.remove(j-2)));
                    arr1.add(String.valueOf(c));
                    break;
                case "/":
                    BigDecimal d=new BigDecimal(arr1.remove(j-2)).divide(new BigDecimal(arr1.remove(j-2)),10,BigDecimal.ROUND_HALF_UP);
                    arr1.add(String.valueOf(d));
                    break;

                default: arr1.add(arr.get(i));
                break;

            }
        }
        Log.d("计算结果",arr1.get(0));
        return arr1.size()==1? arr1.get(0):"calcFailed";

    }
}

