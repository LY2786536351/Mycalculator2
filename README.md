# 安卓计算器Mycalculator2（实验二）

#### 介绍
这是一个完整的计算器，可以实现加减乘除的基本计算，同时可以实现生命周期的监听计算，并且将信息通过日志打印到Logcat中，同时可以将计算结果输出用以查看，还可以把计算记过历史保存。采用API26.

#### 软件架构
软件架构说明


#### 安装教程

直接在Android Studio新建一个项目文件，采用的是Java语言，API选择API26：Android8.0

#### 使用说明

把文件复制到对应的文件夹里就可以复现。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
